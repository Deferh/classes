#include <iostream>
using namespace std;

#include "Demo.h"


using namespace std;

Investment::Investment()
{
	value = 0;
	interest_rate = 0;
}

Investment::Investment(long double value, float interest_rate)
{
	value = value;
	interest_rate = interest_rate;
}

void Investment::setRate(float var)
{
  interest_rate = var;
}

float Investment::getRate()
{
	return interest_rate;
}

void Investment::setVal(long double var2 )
{
  value = var2;
}

long double Investment::getVal()
{
	return value;
}
 
void Investment::Baccount()
{
	value = value + (value * interest_rate);
}
