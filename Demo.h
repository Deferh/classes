#ifndef DEMO_H
#define DEMO_H

class Investment 
 {
    private:  //properties
         long double value;
         float interest_rate; 
         


    public:  //methods
        
         Investment();//default constructor 
         Investment(long double, float);//overloaded constructor 

		  void Baccount();
		  
		  long double getVal();
          void setVal(long double);
			
          float getRate();
          void setRate(float);

 }; //end class
#endif


int main()
{
	Investment demo = Investment(0.083, 100.00);
		
	int count = 0;
			
		while(demo.getVal()<1000000)
		{
			for(int i = 0; i<12; i++)
			{
				demo.setVal(demo.getVal() + 250);
			}
			demo.Baccount();
			count++;
		}
		cout<<" It will take " <<count<<" year(s) to become a millionaire "<<endl;
			
		return 0;
}
